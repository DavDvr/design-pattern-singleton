<?php

namespace App;

require_once (dirname(__DIR__)."/vendor/autoload.php");
final class Singleton
{
    private static $singleton = null;
    private int $compteur=0;

    private function __construct()
    {
        echo "Instanciation du singleton<br>";
    }

    public static function getInstance()
    {
        if (null == self::$singleton){
            try {
                return self::$singleton = new Singleton();

            }catch(\Exception $exception){

                echo "Impossible d'instancier la classe :".$exception->getMessage();
            }

        }
            return self::$singleton;
    }


    public function traiter(string $taskName)
    {
        echo "Traitement de la tache $taskName <br>";
        for ($i = 1; $i <= 5; $i++){
            ++$this->compteur;
            echo ".. ".$this->compteur."<br>";
            sleep(1);
        }
        echo "Fin du traitement de la tache $taskName . compteur = $this->compteur<br>";
    }


}